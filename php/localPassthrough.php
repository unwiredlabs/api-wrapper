<?php
/*
 * Customer-hosted Wrapper to send data to the Unwired Labs LocationAPI
 * 
 * Created: 27th September, 2013 
 *
 */

//Your Unwired Labs LocationAPI token
$token = "";

//Additional information will be printed
define("debug", 0);

if(debug) {
	//Show errors on-screen
	ini_set("display_errors", 1);
} else {
	//Don't show errors on-screen
	ini_set("display_errors", 0);	
}

//Read data from the raw socket
$fp = fopen('php://input', 'r');
$rawData = stream_get_contents($fp);

//Parse into JSON
$parsedData = @json_decode($rawData, true);
if(!$parsedData) {
    $response = '{
                    "status": "error",
                    "message": "Invalid request"
                }';
	print_r($response);
	exit;
}

//Add token to the request & reencode to JSON
$parsedData['token'] = $token;
$jsonData = @json_encode($parsedData);

//Build the request URL
$protocol = "https"; //"https" or "http"
$endpoint = "us1.unwiredlabs.com"; //Check unwiredlabs.com/api for a list of endpoints
$url = "{$protocol}://{$endpoint}/v2/process.php";

//Initiate a cURL to Unwired
$ch = curl_init("{$url}");
curl_setopt_array($ch, array(
    CURLOPT_POST => TRUE,
    CURLOPT_RETURNTRANSFER => TRUE,
    CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json'
    ),
    CURLOPT_POSTFIELDS => $jsonData,
    CURLOPT_TIMEOUT => 5, //seconds
    CURLOPT_CONNECTTIMEOUT => 2, //seconds
	CURLOPT_CAPATH => "../ca-bundle.crt" //Disable when using 'http' protocol
));

// Send the request
$response = curl_exec($ch);

$output = array();

// Check for connection errors
if ($response === FALSE) {
    $output = array(
    	"status" => "error",
		"message" => "connectivity error",
    );
	if(debug) {
		//debug mode only
		$output['debug'] = curl_error($ch);		
	}
	exit(@json_encode($output));
}

// Print output
echo $response;