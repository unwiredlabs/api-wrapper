# Unwired Labs LocationAPI Wrapper #

You can use the wrapper if you would like to expose our LocationAPI internally or to your customers from your own endpoint. The scripts serve as a reverse proxy to the LocationAPI servers.

### Configuration ###

* **API Token** You will have to fill in your API token in the specified section of the script
* **Endpoint** Select an endpoint closest to your servers. Pick from this list of available endpoints.
* **Debug mode** Compiling with `debug` as `1` will print additional info that's helpful for debugging